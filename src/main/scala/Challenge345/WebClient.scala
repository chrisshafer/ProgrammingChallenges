package Challenge345

import java.io.PrintWriter
import java.net.Socket
import scala.io.BufferedSource

object WebClient extends App {

  case class URL(host: String, port: Int, dir: Option[String])

  def parseUrl(urlStr: String) = {
    val regex = """(http:\/\/)?([a-zA-Z\.]*)(:[0-9]*)?(/.*)?""".r
    println(regex.unapplySeq(urlStr))
    urlStr match {
      case regex(_, host, null, directory) => URL(host, 80, Option(directory))
      case regex(_, host, port, directory) => URL(host, port.replace(":","").toInt, Option(directory))
    }
  }

  def get(urlString: String) = {
    val url          = parseUrl(urlString)

    val socketClient = new Socket(url.host, url.port)
    val inputStreeam = new BufferedSource(socketClient.getInputStream).getLines()
    val output       = new PrintWriter(socketClient.getOutputStream)

    output.print(s"GET ${url.dir.getOrElse("/")} HTTP/1.1\r\n")
    output.print(s"Host: ${url.host}\r\n\r\n")

    output.flush()

    while(inputStreeam.hasNext){
      println(inputStreeam.next())
    }

    socketClient.close()
  }

  get(args(0))
}
